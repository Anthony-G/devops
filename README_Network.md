##Définir la zone et région par défaut de l'atelier : 

gcloud config set compute/zone "Zone"
export ZONE=$(gcloud config get compute/zone)

gcloud config set compute/region "Region"
export REGION=$(gcloud config get compute/region)

##Créer le réseau personnalisé
gcloud compute networks create taw-custom-network --subnet-mode custom

##Créer les sous-reseaux
gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.0.0.0/16

   gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.1.0.0/16

   gcloud compute networks subnets create subnet-Region \
   --network taw-custom-network \
   --region Region \
   --range 10.2.0.0/16

##Créer des règles de pare-feu
gcloud compute firewall-rules create nw101-allow-http \
--allow tcp:80 --network taw-custom-network --source-ranges 0.0.0.0/0 \
--target-tags http

##Créer des règles de pare-feu supplémentaires
gcloud compute firewall-rules create "nw101-allow-icmp" --allow icmp --network "taw-custom-network" --target-tags rules

gcloud compute firewall-rules create "nw101-allow-internal" --allow tcp:0-65535,udp:0-65535,icmp --network "taw-custom-network" --source-ranges "10.0.0.0/16","10.2.0.0/16","10.1.0.0/16"

gcloud compute firewall-rules create "nw101-allow-ssh" --allow tcp:22 --network "taw-custom-network" --target-tags "ssh"

gcloud compute firewall-rules create "nw101-allow-rdp" --allow tcp:3389 --network "taw-custom-network"

##Créer les VM
gcloud compute instances create us-test-01 \
--subnet subnet-Region \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-02 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules

gcloud compute instances create us-test-03 \
--subnet subnet-REGION \
--zone ZONE \
--machine-type e2-standard-2 \
--tags ssh,http,rules
